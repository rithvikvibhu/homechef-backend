var express = require('express');
var router = express.Router();

var User     = require('../models/users');
var Food     = require('../models/food');


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

// POST user to be registered
router.post('/register', function(req, res, next) {
  console.log(req.body);
  
  User.find({email: req.body.email}, function(err, dbuser) {
    if (!dbuser.length) {
      var user = new User();
      user.name = req.body.name;
      user.flat = req.body.flat;
      user.email = req.body.email;
      user.password = req.body.password;
      user.approved = true;
      user.save(function(err) {
        res.json({ message: 'User created' });
      });
    } else {
      res.json({ message: 'User already exists' });
    }
  });
  
});

// POST login request
router.post('/login', function(req, res, next) {
  console.log({email: req.body.email, password: req.body.password})
  User.findOne({email: req.body.email, password: req.body.password}, function(err, dbuser) {
    console.log(dbuser)
    if (dbuser && dbuser.approved) {
      var random = Math.random().toString(36).substring(7);
      random = 'test';
      User.update({email: req.body.email}, {token: random}, function(err, num) {
        res.json({token: random});
      });
    } else {
      res.json({ message: 'Could not authenticate' });
    }
  });
});

// POST food?
router.post('/food', function(req, res, next) {
  User.find({token: req.body.token}, function(err, dbusers) {
    if (dbusers.length) {
      Food.find({}, function(err, food) {
          res.json({food: food});
      });
    } else {
      res.json({ message: 'Invalid token' });
    }
  });
});

// POST request to create food. 
router.post('/food/create', function(req, res, next) {
  console.log(req.body);
  User.find({token: req.body.token}, function(err, dbusers) {
    if (dbusers.length) {
      // dbusers[0].email
      var food = new Food();
      food.name = req.body.name;
      food.desc = req.body.desc;
      food.seller = dbusers[0]._id;
      food.ingredients = JSON.parse(req.body.ingredients);
      food.ven = req.body.ven;
      food.deadlineDate = req.body.deadlineDate;
      food.deliveryDate = req.body.deliveryDate;
      food.createdDate = new Date();
      food.cost = req.body.cost;
      food.unit = req.body.unit;
      food.photos = JSON.parse(req.body.photos);
      food.save(function(err) {
        res.json({ message: 'Food created', food: food });
      });
    } else {
      res.json({ message: 'Invalid token' });
    }
  });
});

// POST rrequest to remove food
router.post('/food/remove', function(req, res, next) {
  User.find({token: req.body.token}, function(err, dbusers) {
    if (dbusers.length) {
      Food.find({seller: dbusers[0]._id, _id: req.body.id}, function(err, food) {
        if (err) { console.log(err) }
        if (food.length) {
          Food.remove({_id: req.body.id}, function(err) {
            res.json({ message: 'Removed food', food: food[0] });
          });
        }
      });
    } else {
      res.json({ message: 'Invalid token' });
    }
  });
});

// POST request to get array of User's orders.
router.post('/orders', function(req, res, next) {
  User.find({token: req.body.token}, function(err, dbusers) {
    if (dbusers.length) {
      res.json({orders: dbusers[0].orders});
    } else {
      res.json({ message: 'Invalid token' });
    }
  });
});

// POST request to place order
router.post('/orders/order', function(req, res, next) {
  console.log(req.body);
  var timestamp = new Date();
  User.find({token: req.body.token}, function(err, dbusers) {
    console.log(1, err);
    Food.find({_id: req.body.id}, function(err, dbfood) {
      if (dbfood.length) {
        if (dbusers.length) {
            User.findByIdAndUpdate(dbusers[0]._id, {
              $push: {
                "orders": {
                  id: req.body.id,
                  quantity: req.body.quantity,
                  orderDate: timestamp
                }
              }
            }, {safe: true, new : true}, function(err, model) {
              Food.findByIdAndUpdate(req.body.id, {$inc: {'priv.sold':1}}, function(err, data) {
                console.log(2, err);
                Food.findByIdAndUpdate(req.body.id, {
                  $push: {
                    "priv.orders": {
                      buyer: dbusers[0]._id,
                      quantity: req.body.quantity,
                      orderDate: timestamp
                    }
                  }
                }, {safe: true, new : true}, function(err, model) {
                  console.log(3, err);
                  res.json({ message: 'Ordered successfully' });
                });
              });
            });
        } else {
          res.json({ message: 'Invalid token' });
        }
      } else {
          res.json({ message: 'Food not found' })
        }
      });
  });
});


module.exports = router;
