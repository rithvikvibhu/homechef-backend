var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var GuildSchema   = new Schema({
    name: {
        type: String,
        required: true
    },
    createdDate: Date,
    
    admin: String,  // user ID of the admin. 
    guildType: {
        type: String,
        default: "Community"
    },
    users: Array,   // Array of user IDs
    mods: Array     // Array of moderator user IDs
});

module.exports = mongoose.model('Guild', GuildSchema);