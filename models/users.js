var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
    name: {
        type: String,
        required: true
    },
    createdDate: Date,
    
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: String,
    salt: String,
    hash: String,
    flat: String,
    approved: {
        type: Boolean,
        default: false
    },
    token: String,
    orders: Array // Array of {id: '', quantity: 1}
});

module.exports = mongoose.model('User', UserSchema);