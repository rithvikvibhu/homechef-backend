var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var FoodSchema   = new Schema({
    name: String,
    desc: String,
    seller: String,
    ingredients: Array,
    ven: Number, // 0-veg, 1-egg, 2-nonveg
    deliveryDate: Date,
    deadlineDate: Date,
    createdDate: Date,
    cost: Number,
    unit: String,
    photos: Array,
    priv: {
        sold: Number,
        orders: Array, // Array of {buyer: '', quantity: ''}
        maxorder: Number
    }
});

module.exports = mongoose.model('Food', FoodSchema);